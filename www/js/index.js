/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    // console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    // document.getElementById('deviceready').classList.add('ready');

    $(document).ready(function(){
              
        var form = $("#login-form");
       
        $("#login-form").submit(function(event) {
        event.preventDefault(); // Prevent the form from submitting normally
        var username = $("#username").val();
        var password = $("#password").val();
        console.log("username is:" + username + "and" +"password is:" + password);
        const storedData = JSON.parse(localStorage.getItem("myData"));
        console.log(storedData); // { key1: 'value1', key2: 'value2' }

        

        if (username ==  storedData.value1 && password == storedData.value2 ) {
            window.location.href = 'task.html';

           }else{
            $(".err").css("visibility", "visible");
            //  $(".err").show();
            // $(".err").html("Incorrect Username and Password").css({"color":"red"});
            form[0].reset();

            $("input").on("click", function() {
                // $(".err").html("Incorrect Username and Password").css({"color":"white"});
                // $(".err").hide();
                $(".err").css("visibility", "hidden");
              });


           }
        });

        

      });
}
